package com.firdous.zirotodo.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.firdous.zirotodo.R
import com.firdous.zirotodo.room.entity.TaskModel
import com.firdous.zirotodo.utils.DateUtils


import java.util.ArrayList

class CompletedTaskAdapter(
    private val taskModelList: ArrayList<TaskModel>
) : RecyclerView.Adapter<CompletedTaskAdapter.TaskViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.item_completed_task, parent, false)
        return TaskViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: TaskViewHolder, position: Int) {
        holder.setView()
    }

    override fun getItemCount(): Int {
        return taskModelList.size
    }

    inner class TaskViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val txtTitle : TextView = itemView.findViewById(R.id.txt_task_title)
        private val txtDesc : TextView = itemView.findViewById(R.id.txt_task_desc)
        private val txtTime : TextView = itemView.findViewById(R.id.txt_task_time)

        fun setView() {
            txtTitle.text = taskModelList[adapterPosition].title
            txtDesc.text = taskModelList[adapterPosition].description
            txtTime.text = DateUtils.getDateTimeFromMillis(taskModelList[adapterPosition].time)
        }
    }
}