package com.firdous.zirotodo.activities

import android.annotation.SuppressLint
import android.content.Intent
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.firdous.zirotodo.R
import com.firdous.zirotodo.adapter.SelectTaskAdapter
import com.firdous.zirotodo.dialog.CommonProgressDialog
import com.firdous.zirotodo.room.db.DatabaseClient
import com.firdous.zirotodo.room.entity.TaskModel
import com.firdous.zirotodo.utils.DividerItemDecorationForRecyclerView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_toolbar.*
import java.util.ArrayList


class MainActivity : AppCompatActivity() {

    private lateinit var commonProgressDialog: CommonProgressDialog
    private var taskModelList: ArrayList<TaskModel> = ArrayList()

    private var selectTaskAdapter: SelectTaskAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initUI()
        setListener()
    }

    private fun initUI() {
        txt_header.text = getString(R.string.task_list)
        commonProgressDialog = CommonProgressDialog(this)
    }

    private fun setListener() {
        txt_add_new_task.setOnClickListener { gotoAddNewTask() }
        txt_view_completed.setOnClickListener { gotoCompletedTask() }
        btn_mark_completed.setOnClickListener { MarkTaskCompleted().execute() }
        btn_delete_task.setOnClickListener { DeleteTask().execute() }
    }

    private fun gotoAddNewTask() {
        val intent = Intent(this, AddUpdateTaskActivity::class.java)
        startActivity(intent)
    }
    private fun gotoCompletedTask() {
        val intent = Intent(this, CompletedTaskActivity::class.java)
        startActivity(intent)
    }
    override fun onResume() {
        super.onResume()
        commonProgressDialog.showProgressDialog()
        FetchAllTask().execute()
    }

    @SuppressLint("StaticFieldLeak")
    inner class FetchAllTask : AsyncTask<Void, Void, List<TaskModel>>() {

        override fun doInBackground(vararg p0: Void?): List<TaskModel> {
            return DatabaseClient.getAppDatabase(this@MainActivity)
                .taskDao().fetchAllTasks()
        }

        override fun onPostExecute(result: List<TaskModel>?) {
            super.onPostExecute(result)
            commonProgressDialog.dismissProgressDialog()
            if (result != null && result.isNotEmpty()) {
                taskModelList = result as ArrayList<TaskModel>
                txt_no_task.visibility = View.GONE
                setAdapter()
            } else {
                txt_no_task.visibility = View.VISIBLE
            }

        }
    }

    @SuppressLint("StaticFieldLeak")
    inner class DeleteTask : AsyncTask<Void, Void, Void>() {

        override fun doInBackground(vararg p0: Void?): Void? {
            val taskList: List<TaskModel> = selectTaskAdapter!!.getSelectedTaskList()
            for (taskModel in taskList) {
                if (DatabaseClient.getAppDatabase(this@MainActivity)
                        .taskDao().deleteTask(taskModel) != 0
                ) {
                    runOnUiThread {
                        removeTaskAndNotify(taskModel.id)
                    }

                }
            }
            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
            commonProgressDialog.dismissProgressDialog()
        }

    }


    @SuppressLint("StaticFieldLeak")
    inner class MarkTaskCompleted : AsyncTask<Void, Void, Void>() {

        override fun doInBackground(vararg p0: Void?): Void? {
            val taskList: List<TaskModel> = selectTaskAdapter!!.getSelectedTaskList()
            for (taskModel in taskList) {
                taskModel.completed = true
                if (DatabaseClient.getAppDatabase(this@MainActivity)
                        .taskDao().updateTask(taskModel) != 0
                ) {
                    runOnUiThread {
                        removeTaskAndNotify(taskModel.id)
                    }

                }
            }
            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
            commonProgressDialog.dismissProgressDialog()
        }

    }

    private fun removeTaskAndNotify(id: Int) {
        for (position in taskModelList.indices) {
            if (id == taskModelList[position].id) {
                taskModelList.remove(taskModelList[position])
                selectTaskAdapter!!.notifyItemRemoved(position)
                break
            }
        }
        if(taskModelList.size == 0){
            txt_no_task.visibility = View.VISIBLE
            setButtonVisible(false)
        }
    }

    private fun setAdapter() {
        selectTaskAdapter =
            SelectTaskAdapter(this, taskModelList, TaskSelectedListener())
        rv_task.layoutManager = LinearLayoutManager(this)
        rv_task.addItemDecoration(
            DividerItemDecorationForRecyclerView(
                this,
                LinearLayoutManager.VERTICAL,
                0
            )
        )
        rv_task.adapter = selectTaskAdapter

    }

    inner class TaskSelectedListener : SelectTaskAdapter.TaskSelectionListener {
        override fun onTaskSelected(position: Int) {
            selectTaskAdapter!!.addTaskToSelectedListAndMarkTick(position)
        }

        override fun onDataChange() {
            setButtonVisible(selectTaskAdapter!!.selectedTaskSize > 0)
        }

    }
    private fun setButtonVisible(isVisible: Boolean) {
        if (isVisible) {
            btn_delete_task.visibility = View.VISIBLE
            btn_mark_completed.visibility = View.VISIBLE
        } else {
            btn_delete_task.visibility = View.GONE
            btn_mark_completed.visibility = View.GONE
        }
    }
}
