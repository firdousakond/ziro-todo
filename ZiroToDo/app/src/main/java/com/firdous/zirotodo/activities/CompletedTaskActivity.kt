package com.firdous.zirotodo.activities

import android.annotation.SuppressLint
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.firdous.zirotodo.R
import com.firdous.zirotodo.adapter.CompletedTaskAdapter
import com.firdous.zirotodo.dialog.CommonProgressDialog
import com.firdous.zirotodo.room.db.DatabaseClient
import com.firdous.zirotodo.room.entity.TaskModel
import com.firdous.zirotodo.utils.DividerItemDecorationForRecyclerView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_toolbar.*
import java.util.ArrayList

class CompletedTaskActivity : AppCompatActivity() {

    private lateinit var commonProgressDialog: CommonProgressDialog
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_completed_task)
        initUI()
        setListener()
        FetchCompletedTask().execute()
    }

    private fun initUI() {
        txt_header.text = getString(R.string.completed_task)
        ibtn_back.visibility = View.VISIBLE
        commonProgressDialog = CommonProgressDialog(this)
        commonProgressDialog.showProgressDialog()
    }

    private fun setListener(){
        ibtn_back.setOnClickListener { finish() }
    }
    @SuppressLint("StaticFieldLeak")
    inner class FetchCompletedTask : AsyncTask<Void, Void, List<TaskModel>>() {

        override fun doInBackground(vararg p0: Void?): List<TaskModel> {
            return DatabaseClient.getAppDatabase(this@CompletedTaskActivity)
                .taskDao().fetchCompletedTasks()
        }

        override fun onPostExecute(result: List<TaskModel>?) {
            super.onPostExecute(result)
            commonProgressDialog.dismissProgressDialog()
            if (result != null && result.isNotEmpty()) {
                txt_no_task.visibility = View.GONE
                setAdapter(result)
            } else {
                txt_no_task.visibility = View.VISIBLE
            }

        }

        private fun setAdapter(taskModelList: List<TaskModel>) {
            val completedTaskAdapter =
                CompletedTaskAdapter(taskModelList as ArrayList<TaskModel>)
            rv_task.layoutManager = LinearLayoutManager(this@CompletedTaskActivity)
            rv_task.addItemDecoration(
                DividerItemDecorationForRecyclerView(
                    this@CompletedTaskActivity,
                    LinearLayoutManager.VERTICAL,
                    0
                )
            )
            rv_task.adapter = completedTaskAdapter
        }
    }

}
