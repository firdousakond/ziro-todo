package com.firdous.zirotodo.room.entity

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Entity
@Parcelize
data class TaskModel
    (
    @PrimaryKey(autoGenerate = true)
    val id : Int,
    @ColumnInfo(name = "title")
    val title : String,
    @ColumnInfo(name = "description")
    val description : String,
    @ColumnInfo(name = "time")
    val time : Long,
    @ColumnInfo(name = "completed")
    var completed : Boolean,
    var isTaskSelected : Boolean

) : Parcelable