package com.firdous.zirotodo.room.db

import android.content.Context
import androidx.room.Room

class DatabaseClient {

  companion object {
      fun getAppDatabase(context: Context): TaskDatabase {
          return Room.databaseBuilder(context, TaskDatabase::class.java, "ToDoDB").build()
      }
  }

}