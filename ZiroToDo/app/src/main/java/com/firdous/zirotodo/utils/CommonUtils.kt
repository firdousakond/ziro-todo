package com.firdous.zirotodo.utils

import android.app.Activity
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.firdous.zirotodo.R
import com.google.android.material.snackbar.Snackbar

class CommonUtils {

    companion object {

        fun displaySnackbar(activity: Activity?, message: String, messageType: Int) {

            if (activity != null) {

                var view: View? = activity.window.decorView.rootView ?: return

                try {
                    view =
                        (activity.findViewById<View>(android.R.id.content) as ViewGroup).getChildAt(
                            0
                        )
                } catch (e: Exception) {
                    Log.d("Error",e.message)
                }

                val snackbar = Snackbar.make(view!!, message, Snackbar.LENGTH_SHORT)

                val tvSnackBar =
                    snackbar.view.findViewById<TextView>(com.google.android.material.R.id.snackbar_text)

                tvSnackBar.textAlignment = View.TEXT_ALIGNMENT_CENTER

                when (messageType) {

                    AppConstant.MESSAGE_TYPE_ERROR -> tvSnackBar.setBackgroundColor(
                        activity.resources.getColor(
                            R.color.red_color
                        )
                    )
                    AppConstant.MESSAGE_TYPE_SUCCESS -> tvSnackBar.setBackgroundColor(
                        activity.resources.getColor(
                            R.color.green_color
                        )
                    )
                    else -> tvSnackBar.setBackgroundColor(activity.resources.getColor(R.color.grey_color))
                }

                snackbar.view.setPadding(0, 0, 0, 0)

                snackbar.show()

            }
        }


        fun isNetworkAvailable(activity: Activity): Boolean {

            val connectivity =
                activity.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

            val info = connectivity.allNetworkInfo

            if (info != null) {

                for (anInfo in info) {

                    if (anInfo.state == NetworkInfo.State.CONNECTED) {

                        return true
                    }
                }
            }

            return false

        }
    }

}

