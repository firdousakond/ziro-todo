package com.firdous.zirotodo.utils

interface AppConstant {

    companion object{
        const val MESSAGE_TYPE_SUCCESS = 100
        const val MESSAGE_TYPE_ERROR = 200
        const val TASK_MODEL = "taskModel"
        const val IS_UPDATE = "isUpdate"
    }
}