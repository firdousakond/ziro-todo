package com.firdous.zirotodo.activities

import android.annotation.SuppressLint
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.text.TextUtils
import android.view.View
import com.firdous.zirotodo.R
import com.firdous.zirotodo.dialog.CommonProgressDialog
import com.firdous.zirotodo.dialog.DateTimeDialog
import com.firdous.zirotodo.room.db.DatabaseClient
import com.firdous.zirotodo.room.entity.TaskModel
import com.firdous.zirotodo.utils.AppConstant
import com.firdous.zirotodo.utils.CommonUtils
import com.firdous.zirotodo.utils.DateUtils
import kotlinx.android.synthetic.main.activity_add_task.*
import kotlinx.android.synthetic.main.activity_toolbar.*

class AddUpdateTaskActivity : AppCompatActivity() {

    private var taskTime : Long = 0L
    private lateinit var commonProgressDialog: CommonProgressDialog
    private lateinit var taskModel: TaskModel
    private var isUpdate  = false
    private var id = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_task)
        initUI()
        setListener()
        getIntentData()
    }

    private fun initUI(){
        txt_header.text = getString(R.string.add_new_task)
        ibtn_back.visibility = View.VISIBLE
        commonProgressDialog = CommonProgressDialog(this)
    }

    private fun setListener(){
        txt_date_time.setOnClickListener { setDateClick() }
        btn_save.setOnClickListener { saveTask() }
        ibtn_back.setOnClickListener { finish() }
    }

    private fun getIntentData(){

        if(intent != null && intent.extras != null){
            isUpdate = intent.getBooleanExtra(AppConstant.IS_UPDATE,false)
            taskModel = intent.getParcelableExtra(AppConstant.TASK_MODEL)
            id = taskModel.id
            taskTime = taskModel.time
            et_header.setText(taskModel.title)
            et_description.setText(taskModel.description)
            txt_date_time.text = DateUtils.getDateTimeFromMillis(taskModel.time)
            if(isUpdate){
                txt_header.text = getString(R.string.update_task)
            }
        }
    }

    private fun setDateClick(){
        val dateTimeDialog = DateTimeDialog(this)
        dateTimeDialog.show()
    }

    fun setDateTime(timeInMillis : Long){
        taskTime = timeInMillis
        val dateTime = DateUtils.getDateTimeFromMillis(timeInMillis)
        txt_date_time.text = dateTime
    }

    private fun saveTask(){

        if(isValidFields()){
            commonProgressDialog.showProgressDialog()
            SaveTask().execute()

        }
    }

    private fun isValidFields() : Boolean {

        if (TextUtils.isEmpty(et_header.text)) {
            CommonUtils.displaySnackbar(
                this,
                getString(R.string.invalid_header),
                AppConstant.MESSAGE_TYPE_ERROR
            )
            return false
        }
        if (TextUtils.isEmpty(et_description.text)) {
            CommonUtils.displaySnackbar(
                this,
                getString(R.string.invalid_description),
                AppConstant.MESSAGE_TYPE_SUCCESS
            )
            return false
        }
        if (taskTime == 0L) {
            CommonUtils.displaySnackbar(
                this,
                getString(R.string.invalid_task_date),
                AppConstant.MESSAGE_TYPE_ERROR
            )
            return false
        }

        return true
    }

        @SuppressLint("StaticFieldLeak")
        inner class SaveTask : AsyncTask<Void, Void, Long>() {

        override fun doInBackground(vararg p0: Void?): Long {

            val taskModel = TaskModel(id,et_header.text.toString(),et_description.text.toString(),taskTime,
                completed = false,
                isTaskSelected = false
            )
            return if(isUpdate){
                DatabaseClient.getAppDatabase(this@AddUpdateTaskActivity)
                    .taskDao()
                    .updateTask(taskModel).toLong()
            } else {
                DatabaseClient.getAppDatabase(this@AddUpdateTaskActivity)
                    .taskDao()
                    .insertTask(taskModel)
            }
        }

        override fun onPostExecute(result: Long?) {
            super.onPostExecute(result)
            commonProgressDialog.dismissProgressDialog()
            if(result != 0L){
                CommonUtils.displaySnackbar(this@AddUpdateTaskActivity,getString(R.string.success_task),AppConstant.MESSAGE_TYPE_SUCCESS)
                val handler = Handler()
                handler.postDelayed({
                    finish()
                }, 1000)
            }
            else{
                CommonUtils.displaySnackbar(this@AddUpdateTaskActivity,getString(R.string.error_task),AppConstant.MESSAGE_TYPE_ERROR)
            }

        }
    }

}
