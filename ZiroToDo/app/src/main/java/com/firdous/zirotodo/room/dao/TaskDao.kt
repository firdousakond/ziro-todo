package com.firdous.zirotodo.room.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.firdous.zirotodo.room.entity.TaskModel


@Dao
interface TaskDao {

    @Insert
    fun insertTask(taskModel: TaskModel): Long

    @Query("SELECT * FROM TaskModel WHERE completed = 0 ORDER BY id desc")
    fun fetchAllTasks(): List<TaskModel>

    @Query("SELECT * FROM TaskModel WHERE completed = 1 ORDER BY id desc")
    fun fetchCompletedTasks(): List<TaskModel>

    @Update
    fun updateTask(taskModel: TaskModel) : Int

    @Delete
    fun deleteTask(taskModel: TaskModel) : Int

}