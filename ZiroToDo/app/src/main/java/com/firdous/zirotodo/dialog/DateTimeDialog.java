package com.firdous.zirotodo.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.TimePicker;

import com.firdous.zirotodo.R;
import com.firdous.zirotodo.activities.AddUpdateTaskActivity;

import java.util.Calendar;

public class DateTimeDialog extends AlertDialog {

    private final Activity activity;
    private TimePicker timePicker;
    private DatePicker dateDialogPicker;
    private Button btnOk;
    private TextView txtHeader;
    private boolean isFirst;
    private ImageButton ibtnBack;

    public DateTimeDialog(Activity activity) {
        super(activity);
        isFirst = true;
        this.activity = activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_date_time);
        if (getWindow() != null) {
            getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        iniUI();
    }

    private void iniUI() {

        timePicker = findViewById(R.id.timePicker);
        timePicker.setVisibility(View.GONE);
        dateDialogPicker = findViewById(R.id.datePicker);
        btnOk = findViewById(R.id.btn_okay);

        ibtnBack = findViewById(R.id.ibtn_back);
        txtHeader = findViewById(R.id.txt_header);
        txtHeader.setText(activity.getString(R.string.select_date));

        initListener();
    }

    private void initListener() {

        ibtnBack.setOnClickListener(new ImageButtonBackOnClickListener());
        btnOk.setOnClickListener(new ButtonOkayOnClickListener());
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        dateDialogPicker.setMinDate(Calendar.getInstance().getTimeInMillis());
    }

    private class ImageButtonBackOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {

            if (isFirst) {
                dismiss();
            } else {
                isFirst = true;
                dateDialogPicker.setVisibility(View.VISIBLE);
                timePicker.setVisibility(View.GONE);
                txtHeader.setText(activity.getString(R.string.select_date));
            }
        }
    }

    private class ButtonOkayOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {

            if (isFirst) {
                isFirst = false;
                dateDialogPicker.setVisibility(View.GONE);
                timePicker.setVisibility(View.VISIBLE);
                timePicker.setCurrentMinute(timePicker.getCurrentMinute());
                timePicker.setCurrentHour(timePicker.getCurrentHour());
                txtHeader.setText(activity.getString(R.string.select_time));
            } else {
                Calendar calendar = Calendar.getInstance();
                calendar.set(dateDialogPicker.getYear(), dateDialogPicker.getMonth(), dateDialogPicker.getDayOfMonth(), timePicker.getCurrentHour(), timePicker.getCurrentMinute());

                long timeInMillis = calendar.getTimeInMillis();

                if (activity instanceof AddUpdateTaskActivity) {
                    ((AddUpdateTaskActivity) activity).setDateTime(timeInMillis);
                }
                dismiss();

            }
        }
    }
}
