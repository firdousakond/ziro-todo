package com.firdous.zirotodo.room.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.firdous.zirotodo.room.dao.TaskDao
import com.firdous.zirotodo.room.entity.TaskModel


@Database(entities = [TaskModel::class], version = 1, exportSchema = false)
abstract class TaskDatabase : RoomDatabase() {

    abstract fun taskDao(): TaskDao
}