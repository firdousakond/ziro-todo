package com.firdous.zirotodo.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateUtils {

    private static final String DATE_TIME_FORMAT = "dd/MM/y hh:mm aa";

    private static Date toDateFromUnixTimeStamp(long unixTime) {
        return new Date((unixTime));
    }

    public static String getDateTimeFromMillis(long dateInMillis) {
        Date dateGiven = DateUtils.toDateFromUnixTimeStamp(dateInMillis);
        SimpleDateFormat formatter = new SimpleDateFormat(DATE_TIME_FORMAT, Locale.ENGLISH);
        return formatter.format(dateGiven);
    }
}
