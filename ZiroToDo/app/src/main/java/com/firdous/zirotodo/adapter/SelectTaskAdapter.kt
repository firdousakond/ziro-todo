package com.firdous.zirotodo.adapter

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.firdous.zirotodo.R
import com.firdous.zirotodo.activities.AddUpdateTaskActivity
import com.firdous.zirotodo.room.entity.TaskModel
import com.firdous.zirotodo.utils.AppConstant
import com.firdous.zirotodo.utils.DateUtils


import java.util.ArrayList

class SelectTaskAdapter(
    private val activity: Activity,
    private val taskModelList: ArrayList<TaskModel>,
    private val taskSelectedListener: TaskSelectionListener
) : RecyclerView.Adapter<SelectTaskAdapter.TaskViewHolder>() {

    val selectedTaskList: ArrayList<TaskModel> = ArrayList()
    var selectedTaskSize: Int = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.item_task, parent, false)
        return TaskViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: TaskViewHolder, position: Int) {
        holder.setView()
    }

    override fun getItemCount(): Int {
        return taskModelList.size
    }

    fun addTaskToSelectedListAndMarkTick(position: Int) {
        addSelectedTask(position)
        notifyItemChanged(position)
    }

    fun getSelectedTaskList() : List<TaskModel>{
        return selectedTaskList
    }

    private fun addSelectedTask(position: Int) {
        if (!isTaskAlreadySelected(taskModelList[position].id, selectedTaskList)) {
            val taskTypeModel = taskModelList[position]
            taskTypeModel.isTaskSelected = true
            selectedTaskList.add(0, taskTypeModel)
            taskModelList[position] = taskTypeModel
            selectedTaskSize++
        }
    }

    private fun isTaskAlreadySelected(taskId: Int, taskTypeModelsList: List<TaskModel>): Boolean {

        for (taskTypeModel in taskTypeModelsList) {
            if (taskTypeModel.id == taskId) {
                return true
            }
        }
        return false
    }

    interface TaskSelectionListener {
        fun onTaskSelected(position: Int)
        fun onDataChange()
    }

    inner class TaskViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val txtTitle : TextView = itemView.findViewById(R.id.txt_task_title)
        private val txtDesc : TextView = itemView.findViewById(R.id.txt_task_desc)
        private val txtTime : TextView = itemView.findViewById(R.id.txt_task_time)
        private val imgTick : ImageView = itemView.findViewById(R.id.img_tick)
        private val imgEdit : ImageView = itemView.findViewById(R.id.img_edit)

        init {
            imgEdit.setOnClickListener {
                val intent = Intent(activity,AddUpdateTaskActivity::class.java)
                intent.putExtra(AppConstant.TASK_MODEL,taskModelList[adapterPosition])
                intent.putExtra(AppConstant.IS_UPDATE,true)
                activity.startActivity(intent)
            }
            itemView.setOnClickListener {

                val prevValue = taskModelList[adapterPosition].isTaskSelected
                when {
                    prevValue -> {
                        taskModelList[adapterPosition].isTaskSelected = false
                        selectedTaskList.remove(taskModelList[adapterPosition])
                        notifyDataSetChanged()
                        selectedTaskSize--
                    }
                    else -> taskSelectedListener.onTaskSelected(adapterPosition)
                }
                taskSelectedListener.onDataChange()

            }
        }

        fun setView() {
            txtTitle.text = taskModelList[adapterPosition].title
            txtDesc.text = taskModelList[adapterPosition].description
            txtTime.text = DateUtils.getDateTimeFromMillis(taskModelList[adapterPosition].time)

            if (taskModelList[adapterPosition].isTaskSelected) {
                addSelectedTask(adapterPosition)
                imgTick.visibility = View.VISIBLE
                imgEdit.visibility = View.INVISIBLE
                txtTitle.setTextColor(ContextCompat.getColor(activity,R.color.yellow_color))
                txtDesc.setTextColor(ContextCompat.getColor(activity,R.color.yellow_color))
                txtTime.setTextColor(ContextCompat.getColor(activity,R.color.yellow_color))
            } else {
                imgTick.visibility = View.INVISIBLE
                imgEdit.visibility = View.VISIBLE
                txtTitle.setTextColor(ContextCompat.getColor(activity,R.color.secondary_color))
                txtDesc.setTextColor(ContextCompat.getColor(activity,R.color.secondary_color))
                txtTime.setTextColor(ContextCompat.getColor(activity,R.color.secondary_color))
            }
        }
    }
}